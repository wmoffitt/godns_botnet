package cli

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"github.com/willmfftt/godns_botnet/internal/pkg/botnet"
	"os"
)

var BotCmd = &cobra.Command{
	Use: "bot",
	Short: "Actions for a bot",
}

var botListCmd = &cobra.Command{
	Use: "list",
	Short: "List all bots",
	Run: func(cmd *cobra.Command, args []string) {
		listBots()
	},
}

var botSetCmd = &cobra.Command{
	Use: "set [bot id]",
	Short: "Set a bot field value",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			if err := cmd.Usage(); err != nil {
				fmt.Println(err)
			}
			os.Exit(1)
		}

		if botId, err := uuid.Parse(args[0]); err != nil {
			fmt.Println(err)
			os.Exit(1)
		} else {
			setBotField(botId)
		}
	},
}

var botDeleteCmd = &cobra.Command{
	Use: "delete [bot id]",
	Short: "Delete a bot",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			if err := cmd.Usage(); err != nil {
				fmt.Println(err)
			}
			os.Exit(1)
		}

		if botId, err := uuid.Parse(args[0]); err != nil {
			fmt.Println(err)
			os.Exit(1)
		} else {
			deleteBot(botId)
		}
	},
}

var botName string

func init() {
	botSetCmd.Flags().StringVarP(&botName, "name", "n", "", "Bot name")

	BotCmd.AddCommand(botListCmd)
	BotCmd.AddCommand(botSetCmd)
	BotCmd.AddCommand(botDeleteCmd)
	RootCmd.AddCommand(BotCmd)
}

func listBots() {
	if bots := botnet.GetBots(); bots != nil {
		for _, bot := range bots {
			botName := "<None>"
			botIPAddress := "<None>"
			commandsPending := 0

			if botCommands := botnet.GetBotCommands(bot.BotId); botCommands != nil {
				commandsPending = len(botCommands)
			}
			if len(bot.Name) > 0 {
				botName = bot.Name
			}
			if len(bot.IPAddress) > 0 {
				botIPAddress = bot.IPAddress
			}

			fmt.Printf("Bot Id: %s, Name: %s, IP Address: %s, Commands Pending: %d\n",
				bot.BotId, botName, botIPAddress, commandsPending)
		}
	} else {
		fmt.Println("No bots available")
	}
}

func setBotField(botId uuid.UUID) {
	if len(botName) > 0 {
		if bot := botnet.GetBot(botId); bot != nil {
			bot.Name = botName
			bot.Save()
			fmt.Println("Bot name successfully changed")
		} else {
			fmt.Println("Bot not found")
		}
	} else {
		fmt.Println("Nothing to change")
	}
}

func deleteBot(botId uuid.UUID) {
	if bot := botnet.GetBot(botId); bot != nil {
		bot.Delete()
		fmt.Println("Bot deleted successfully")
	} else {
		fmt.Println("Bot not found")
	}
}