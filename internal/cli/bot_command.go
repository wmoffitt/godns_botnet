package cli

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"github.com/willmfftt/godns_botnet/internal/pkg/botnet"
	"os"
	"strconv"
)

var botCommandCmd = &cobra.Command{
	Use: "command",
	Short: "Actions for a bot command",
}

var botCommandAvailableCmd = &cobra.Command{
	Use: "available",
	Short: "List available bot commands",
	Run: func(cmd *cobra.Command, args []string) {
		listAvailableBotCommands()
	},
}

var botCommandCreateCmd = &cobra.Command{
	Use: "create [command id] <bot id>",
	Short: "Create a command for all bots. Set <bot id> to create command for a specific bot",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			if err := cmd.Usage(); err != nil {
				fmt.Println(err)
			}
			os.Exit(1)
		}

		if commandId, err := strconv.Atoi(args[0]); err != nil {
			fmt.Println(err)
			os.Exit(1)
		} else {
			if len(args) > 1 {
				if botId, err := uuid.Parse(args[1]); err != nil {
					fmt.Println(err)
					os.Exit(1)
				} else {
					createBotCommand(commandId, &botId)
				}
			} else {
				createBotCommand(commandId, nil)
			}
		}
	},
}

var botCommandListCmd = &cobra.Command{
	Use: "list [bot id]",
	Short: "List pending commands for a bot",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			if err := cmd.Usage(); err != nil {
				fmt.Println(err)
			}
			os.Exit(1)
		}

		if botId, err := uuid.Parse(args[0]); err != nil {
			fmt.Println(err)
			os.Exit(1)
		} else {
			listBotCommands(botId)
		}
	},
}

var commandArgs string

func init() {
	botCommandCreateCmd.Flags().StringVarP(&commandArgs, "args", "a",
		"", "Bot command args")

	botCommandCmd.AddCommand(botCommandAvailableCmd)
	botCommandCmd.AddCommand(botCommandCreateCmd)
	botCommandCmd.AddCommand(botCommandListCmd)
	BotCmd.AddCommand(botCommandCmd)
}

func listAvailableBotCommands() {
	for _, command := range botnet.CommandsOrdered {
		fmt.Printf("Command Id: %d, Command Name: %s, Command Description: %s\n",
			command.Id, command.Name, command.Description)
	}
}

func createBotCommand(commandId int, botId *uuid.UUID) {
	var bots []botnet.Bot
	if botId != nil {
		if bot := botnet.GetBot(*botId); bot != nil {
			bots = append(bots, *bot)
		} else {
			fmt.Println("Bot not found")
			os.Exit(0)
		}
	} else {
		if uncheckedBots := botnet.GetBots(); uncheckedBots != nil {
			bots = uncheckedBots
		} else {
			fmt.Println("No bots available for command")
			os.Exit(0)
		}
	}

	for _, bot := range bots {
		botnet.NewBotCommand(bot.BotId, commandId, commandArgs)
	}
	fmt.Println("Bot command created successfully")
}

func listBotCommands(botId uuid.UUID) {
	if bot := botnet.GetBot(botId); bot != nil {
		if botCommands := botnet.GetBotCommands(botId); botCommands != nil {
			for _, botCommand := range botCommands {
				commandArgs := ""
				if len(botCommand.Args) > 0 {
					commandArgs = botCommand.Args
				}
				if commandName, ok := botnet.Commands[botCommand.CommandId]; ok {
					fmt.Printf("Command Id: %d, Command Name: %s, Command Args: %s\n",
						botCommand.CommandId, commandName.Name, commandArgs)
				}
			}
		} else {
			fmt.Println("No pending commands for bot")
		}
	} else {
		fmt.Println("Bot not found")
	}


}