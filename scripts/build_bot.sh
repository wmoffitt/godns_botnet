#!/bin/bash

packr clean
packr
go build -ldflags="-s -w" -o builds/bot cmd/bot/main.go
packr clean
